//
//  DetalhesViewController.swift
//  Contato
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit
protocol DetalheViewControllerDelegate {
    func excluirContato( index: Int)
    }


class DetalhesViewController: UIViewController {

    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var numeroLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var enderecoLabel: UILabel!
    
    public var contato: Contato?
    public var index: Int?
    public var delegate: DetalheViewControllerDelegate?
    
    
    override func viewDidLoad() {
        nomeLabel.text = contato?.nome
        numeroLabel.text = contato?.telefone
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
        title = contato?.nome
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func excluirContato(_ sender: Any) {
        delegate?.excluirContato(index: index!)
        navigationController?.popViewController(animated: true)
    }
    


}
